FROM node:13-alpine as build

WORKDIR /app
COPY ./package.json package.json
RUN npm install
COPY ./ ./
RUN npm run build

EXPOSE 3000
ENTRYPOINT [ "npm", "start" ]